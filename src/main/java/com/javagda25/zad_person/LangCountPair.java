package com.javagda25.zad_person;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LangCountPair {
    String lang;
    long count;
}
