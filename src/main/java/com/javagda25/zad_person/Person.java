package com.javagda25.zad_person;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Person {
    private String firstName, lastName;
    private int age;
    private boolean isMale;
}
