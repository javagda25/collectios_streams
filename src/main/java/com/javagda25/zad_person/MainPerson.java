package com.javagda25.zad_person;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MainPerson {
    public static void main(String[] args) {
        Person person2 = new Person("Jacek", "Górski", 15, true);
        Person person3 = new Person("Andżelika", "Dżoli", 25, false);
        Person person4 = new Person("Wanda", "Ibanda", 12, false);
        Person person5 = new Person("Marek", "Marecki", 17, true);
        Person person6 = new Person("Johny", "Brawo", 25, true);
        Person person1 = new Person("Jacek", "Kowalski", 18, true);
        Person person7 = new Person("Stary", "Pan", 80, true);
        Person person8 = new Person("Newbie", "Noob", 12, true);
        Person person9 = new Person("Newbies", "Sister", 19, false);
        List<Person> list = new ArrayList<Person>(Arrays.asList(
                person1,
                person2,
                person3,
                person4,
                person5,
                person6,
                person7,
                person8,
                person9
        ));

        // Zadanie - uzyskaj listę mężczyzn
        // gromadzę osoby do innej listy
        List<Person> people = list.stream()
                .filter(person -> person.isMale())
                .collect(Collectors.toList());

        // nową listę wypisuję
        people.forEach(person -> System.out.println(person));
        System.out.println();

        // Zadanie - uzyskaj listę dorosłych kobiet
        // gromadzę osoby do innej listy
        List<Person> peoplekobitki = list.stream()
                .filter(person -> !person.isMale() && person.getAge() >= 18)
                .collect(Collectors.toList());

        // nową listę wypisuję
        peoplekobitki.forEach(person -> System.out.println(person));

        // uzyskaj Optional<Person> z dorosłym Jackiem findAny/findfirst
        Optional<Person> doroslyJacek = list.stream()
                .filter(person -> person.getAge() >= 18 && person.getFirstName().equalsIgnoreCase("Jacek"))
                .findFirst();
        if (doroslyJacek.isPresent()) {
            // wypisz obiekt dorosłego Jacka jeśli go znaleźliśmy
            System.out.println(doroslyJacek.get());
        }

        Set<String> namesCollection = list.stream()
                .map(person -> person.getFirstName())
                .collect(Collectors.toSet());

        Set<String> lastNamesCollection = list.stream()
                .map(person -> person.getLastName())
                .collect(Collectors.toSet());

        // lista wartości integer
        List<Integer> ages = list.stream()
                .map(person -> person.getAge()).collect(Collectors.toList());

        // stream integer -
        OptionalDouble average = list.stream()
//                .filter(person -> person.getLastName().startsWith("XYZ")) // pusty stream po odfiltrowaniu, wtedy optional będzie empty
                .mapToInt(person -> person.getAge()).average();
        if (average.isPresent()) {
            System.out.println("Średnia wieku: " + average.getAsDouble());
        }

        boolean czyJestOsobaKtoraMa100Lat = list.stream().anyMatch(person -> person.getAge() == 100);


        OptionalInt optionalInt= list.stream()
                .mapToInt(per -> per.getAge()).max();

        if(optionalInt.isPresent()){
            int maxAge = optionalInt.getAsInt();

            Optional<Person> person = list.stream().filter(per->per.getAge() == maxAge).findFirst();

            if(person.isPresent()){
                System.out.println(person.get());
            }
        }

    }
}
