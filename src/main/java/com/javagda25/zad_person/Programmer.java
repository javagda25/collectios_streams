package com.javagda25.zad_person;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Programmer{
    private Person person;
    private List<String> languages;
}