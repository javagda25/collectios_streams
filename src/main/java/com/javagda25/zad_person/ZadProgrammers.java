package com.javagda25.zad_person;

import java.util.*;
import java.util.stream.Collectors;

public class ZadProgrammers {
    public static void main(String[] args) {
        Person person1 = new Person("Jacek", "Kowalski", 18, true);
        Person person2 = new Person("Jacek", "Górski", 15, true);
        Person person3 = new Person("Andżelika", "Dżoli", 25, false);
        Person person4 = new Person("Wanda", "Ibanda", 12, false);
        Person person5 = new Person("Marek", "Marecki", 17, true);
        Person person6 = new Person("Johny", "Brawo", 25, true);
        Person person7 = new Person("Stary", "Pan", 80, true);
        Person person8 = new Person("Newbie", "Noob", 12, true);
        Person person9 = new Person("Newbies", "Sister", 19, false);
        List<String> languages1 = Arrays.asList("Java;Cobol;Cpp;Lisp".split(";"));
        List<String> languages2 = Arrays.asList("Java;Lisp".split(";"));
        List<String> languages3 = Arrays.asList("Java;Cobol;Cpp;Lisp;C#".split(";"));
        List<String> languages4 = Arrays.asList("C#;C;Cpp".split(";")); // C# C Cpp
        List<String> languages5 = Arrays.asList("Java;Assembler;Scala;Cobol".split(";"));
        List<String> languages6 = Arrays.asList("Java;Scala".split(";"));
        List<String> languages7 = Arrays.asList("C#;C".split(";"));
        List<String> languages8 = Collections.emptyList();
        List<String> languages9 = Arrays.asList("Java");
        Programmer programmer1 = new Programmer(person1, languages1);
        Programmer programmer2 = new Programmer(person2, languages2);
        Programmer programmer3 = new Programmer(person3, languages3);
        Programmer programmer4 = new Programmer(person4, languages4);
        Programmer programmer5 = new Programmer(person5, languages5);
        Programmer programmer6 = new Programmer(person6, languages6);
        Programmer programmer7 = new Programmer(person7, languages7);
        Programmer programmer8 = new Programmer(person8, languages8);
        Programmer programmer9 = new Programmer(person9, languages9);
        List<Programmer> programmers = Arrays.asList(programmer1, programmer2, programmer3, programmer4, programmer5, programmer6, programmer7, programmer8, programmer9);
        System.out.println(programmers);

        // a
        List<Programmer> mezczyzni = programmers.stream()
                .filter(programmer -> programmer.getPerson().isMale())
                .collect(Collectors.toList());


        // b
        List<Programmer> cobolers = programmers.stream()
                .filter(programmer -> programmer.getPerson().getAge() < 18)
                .filter(programmer -> programmer.getLanguages().contains("Cobol"))
                .collect(Collectors.toList());

        // c
        List<Programmer> kumaci = programmers.stream()
                .filter(programmer -> programmer.getLanguages().size() > 1)
                .collect(Collectors.toList());

        // d
        List<Programmer> cppLaski = programmers.stream()
                .filter(programmer -> !programmer.getPerson().isMale())
                .filter(programmer -> programmer.getLanguages().contains("Java") && programmer.getLanguages().contains("Cpp"))
                .collect(Collectors.toList());

        // e
        List<String> meskieImiona = programmers.stream()
                .filter(programmer -> programmer.getPerson().isMale())
                .map(programmer -> programmer.getPerson().getFirstName()).collect(Collectors.toList());

        // f
        Set<String> languages = programmers.stream()
                .map(programmer -> programmer.getLanguages())
                .flatMap(langu -> langu.stream())
                .collect(Collectors.toSet());

        // g
        List<String> nazwiska = programmers.stream()
                .filter(programmer -> programmer.getLanguages().size() > 2)
                .map(programmer -> programmer.getPerson().getLastName())
                .collect(Collectors.toList());

        // h
        boolean czyIstniejeNiekumata = programmers.stream()
                .anyMatch(programmer -> programmer.getLanguages().size() < 1);

        // i
        long iloscJezykowProgramistek = programmers.stream()
                .filter(programmer -> !programmer.getPerson().isMale())
                .map(programmer -> programmer.getLanguages())
                .flatMap(langu -> langu.stream())
                .distinct()
                .count();

        // j
        boolean czyIstniejeBrawo = programmers.stream()
                .anyMatch(programmer -> programmer.getPerson().getLastName().equalsIgnoreCase("Brawo") && programmer.getLanguages().contains("Java"));

        // k
        // Stream<List<String>>
        // Stream<String>
        //
        // distinct :
        // przed [ a, a, b, c, d, f, f, g, z]
        // po    [ a, b, c, d, f, g, z]
        List<String> languagesList = programmers.stream()
                .map(programmer -> programmer.getLanguages())
                .flatMap(lang -> lang.stream())
                .collect(Collectors.toList());

        OptionalLong min = programmers.stream()
                .map(programmer -> programmer.getLanguages())
                .flatMap(lang -> lang.stream())
                .mapToLong(jezyk -> languagesList.stream().filter(lang -> lang.equalsIgnoreCase(jezyk)).count())
                .min();

        if(min.isPresent()){
            Optional<LangCountPair> langCountPair = programmers.stream()
                    .map(programmer -> programmer.getLanguages())
                    .flatMap(lang -> lang.stream())
                    .map(jezyk -> new LangCountPair(jezyk, languagesList.stream().filter(lang -> lang.equalsIgnoreCase(jezyk)).count()))
                    .filter(jezyk-> jezyk.count == min.getAsLong())
                    .findFirst();
        }

        //

    }
}
